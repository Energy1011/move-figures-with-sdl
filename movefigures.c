/*
 * movefigures.c
 *
 * Copyright (C) 2012 - Alejandro Anaya (energy1011@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//Compilation command:
//gcc movefigures.c -o a.out `sdl-config --cflags --libs` -lSDL_image

#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>

#define MAX_WIDTH_IMG_SIZE 100
#define MAX_HIGH_IMG_SIZE 100
#define MOVING_SIZE 50

#define WIDTH_SCREEN_SIZE 800
#define HIGH_SCREEN_SIZE 600

#define FPS (1000/24)

enum {
	TRIANGLE,
	CIRCLE,
	SQUARE,
		NUM_FIGURES
};

enum {
	ENABLE,
	UNABLE,
		FIGURE_FOCUS_STATE
};

const char *images_figures[NUM_FIGURES] = {
	"images/triangle.png",
	"images/circle.png",
	"images/square.png"
};

const char *images_figures_focus[NUM_FIGURES] = {
	"images/triangleFocus.png",
	"images/circleFocus.png",
	"images/squareFocus.png"
};

typedef struct  _Figure
{
	struct _Figure *next;
	struct _Figure *prev;
	int x;
	int y;
	int pivotX;
	int pivotY;
	int w;
	int h;
	int type;
	int focus;
	int mirror;
	int mirror_corner;
	int mirrorX;
	int mirrorY;

} Figure;

// global declarations for SDL
 SDL_Surface *image, *screen;
 SDL_Rect dest;
 SDL_Event event;

 //pointers for list
 Figure *first_figure = NULL;
 Figure *last_figure = NULL;


int init_SDL()
{
// init the SDL
 if (SDL_Init(SDL_INIT_VIDEO) < 0) {
  printf("No se pudo iniciar SDL: %s\n",SDL_GetError());
  exit(1);
 }
 // Video Mode
 screen = SDL_SetVideoMode(800,600,24,SDL_HWSURFACE);
 if (screen == NULL) {
  printf("No se puede inicializar el modo gráfico: \n",SDL_GetError());
  exit(1);
 }

return 0;
}

int set_SDL_BlitSurface(Figure *SelectFigure)
{

	dest.x = SelectFigure-> x;
	   			 dest.y = SelectFigure-> y;
				 dest.w = SelectFigure-> w;
				 dest.h = SelectFigure-> h;

		    	SDL_BlitSurface(image, NULL, screen, &dest);

	// Mirrors Figures ++
				   if(SelectFigure->mirror == ENABLE)
				   {
				   		 dest.x = SelectFigure-> mirrorX;
			   			 dest.y = SelectFigure-> mirrorY;
						 dest.w = SelectFigure-> w;
						 dest.h = SelectFigure-> h;

						 SDL_BlitSurface(image, NULL, screen, &dest);
				   }
					// check for corner mirror
					    if(SelectFigure->mirror_corner == ENABLE)
					    {
					    	//x=0 y=0 left upper corner
					    	 dest.x = 0 - MOVING_SIZE;
				   			 dest.y = 0 - MOVING_SIZE;
							 dest.w = SelectFigure-> w;
							 dest.h = SelectFigure-> h;

							 SDL_BlitSurface(image, NULL, screen, &dest);

							 //x= WIDTH_SCREEN_SIZE y=0 right upper corner
					    	 dest.x = WIDTH_SCREEN_SIZE - MOVING_SIZE;
				   			 dest.y = 0 - MOVING_SIZE;
							 dest.w = SelectFigure-> w;
							 dest.h = SelectFigure-> h;

							 SDL_BlitSurface(image, NULL, screen, &dest);


							 //x= WIDTH_SCREEN_SIZE y=HIGH_SCREEN SIZE right lower corner
					    	 dest.x = WIDTH_SCREEN_SIZE - MOVING_SIZE;
				   			 dest.y = HIGH_SCREEN_SIZE - MOVING_SIZE;
							 dest.w = SelectFigure-> w;
							 dest.h = SelectFigure-> h;

							 SDL_BlitSurface(image, NULL, screen, &dest);


							 //x=0 y=HIHG_SCREEN_SIZE left lower corner
					    	 dest.x = 0 - MOVING_SIZE;
				   			 dest.y = HIGH_SCREEN_SIZE - MOVING_SIZE;
							 dest.w = SelectFigure-> w;
							 dest.h = SelectFigure-> h;

							 SDL_BlitSurface(image, NULL, screen, &dest);
						}
}

void draw_last_figure_with_focus()
{
	int e;
	Figure *SelectFigure = NULL;

for (e=0; e < NUM_FIGURES; e++)
	{

			//Select a figure from list
		    if(SelectFigure == NULL){
		    	SelectFigure = first_figure;
		    }

		 if(SelectFigure-> focus == ENABLE)
		 {

			 image = IMG_Load(images_figures_focus[e]);

		 }

	    if ( image == NULL) {
	        printf("No pude cargar gráfico: %s\n", SDL_GetError());
	        exit(1);
	    }else{
	    			if(SelectFigure-> focus == ENABLE)
		 			{
			    	set_SDL_BlitSurface(SelectFigure);
			    	}

	    }
	    //Forward the pointer cursor
		    	SelectFigure = SelectFigure-> next;

	}

}


void load_figures()
{
	int e;
	Figure *SelectFigure = NULL;

	while(e < NUM_FIGURES)
	{
			//Select a figure from list
		    if(SelectFigure == NULL){
		    	SelectFigure = first_figure;
		    }

		 if(SelectFigure-> focus == UNABLE)
		 {

			 // load the IMG file
			 image = IMG_Load(images_figures[e]);

		 }else{
			 image = IMG_Load(images_figures_focus[e]);
			}


	    if ( image == NULL ) {
	        printf("No pude cargar gráfico: %s\n", SDL_GetError());
	        exit(1);
	    }else{

			    	set_SDL_BlitSurface(SelectFigure);
				    e++;
				    // // free the surface
   					SDL_FreeSurface(image);
	    }

	   			 //Forward the pointer cursor
		    	SelectFigure = SelectFigure-> next;

	}
	draw_last_figure_with_focus();
	// free the surface
   	SDL_FreeSurface(image);
return;
}


int initialize_figures()
{
	int e;
	Figure *newFigure;

	for (e = 0; e < NUM_FIGURES; e++)
	{
		newFigure = (Figure *) malloc (sizeof (Figure));

		//initialize the FIGURE
		switch(e)
		{
			case TRIANGLE:
				newFigure->x = 250;
				newFigure->y = 200;
				newFigure->mirrorX = newFigure->x;
				newFigure->mirrorY = newFigure->y;
				newFigure->pivotX = newFigure->x;
				newFigure->pivotY = newFigure->y;
				newFigure->type = TRIANGLE;
				newFigure->focus = ENABLE;
				newFigure->mirror = UNABLE;
				newFigure->mirror_corner = UNABLE;
			break;

			case CIRCLE:
				newFigure->x = 350;
				newFigure->y = 200;
				newFigure->mirrorX = newFigure->x;
				newFigure->mirrorY = newFigure->y;
				newFigure->pivotX = newFigure->x;
				newFigure->pivotY = newFigure->y;
				newFigure->type = CIRCLE;
				newFigure->focus = UNABLE;
				newFigure->mirror = UNABLE;
				newFigure->mirror_corner = UNABLE;
			break;

			case SQUARE:
				newFigure->x = 300;
				newFigure->y = 300;
				newFigure->mirrorX = newFigure->x;
				newFigure->mirrorY = newFigure->y;
				newFigure->pivotX = newFigure->x;
				newFigure->pivotY = newFigure->y;
				newFigure->type = SQUARE;
				newFigure->focus = UNABLE;
				newFigure->mirror = UNABLE;
				newFigure->mirror_corner = UNABLE;
			break;
		}
		// set the max width & high size
		newFigure->w = MAX_WIDTH_IMG_SIZE;
		newFigure->h = MAX_HIGH_IMG_SIZE;

		//Add to the list
		newFigure-> next = NULL;
		newFigure-> prev = last_figure;

		if(last_figure == NULL){
			first_figure = last_figure = newFigure;
		} else {
			last_figure->next = newFigure;
			last_figure = newFigure;
		}

	}
	return 0;
}

// just for programming check
void display_list()
{
	Figure *tempFigure=first_figure;
	do{
	printf("x:%i y:%i type:%i \n",tempFigure->x,tempFigure->y,tempFigure->type);
	tempFigure=tempFigure->next;
	}while(tempFigure != NULL);

}

void move_focus()
{
	int e;
	Figure *tempFigure = first_figure;

	for (e = 0; e < NUM_FIGURES; e++)
	{
		if(tempFigure->focus == ENABLE)
		{
			tempFigure->focus = UNABLE;

				// check for last and first figure link
				if(tempFigure->next == NULL)
				{
					tempFigure = first_figure;
					tempFigure->focus = ENABLE;
					return;
				}
				else{
					//move at the next
					tempFigure = tempFigure->next;
					// Set focus value
					tempFigure->focus = ENABLE;
					return;
				}
				if(tempFigure->prev == NULL)
				{
					//move at the last one
				   tempFigure = last_figure;
					// Set focus value
					tempFigure->focus = ENABLE;
					return;
				}

		}
		//advance forward over the list
		tempFigure=tempFigure->next;
	}

}

void draw_figures()
{
	SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,0,0,0));
 	load_figures();
 	// Show the screen
	SDL_Flip(screen);
}


void move_figure(int direction)
{
	int e;
	Figure *tempFigure=first_figure;

for (e = 0; e < NUM_FIGURES; e++)
	{
		if(tempFigure->focus == ENABLE)
		{
				switch(direction){
					case SDLK_UP:
					tempFigure->y-=MOVING_SIZE;
					break;

					case SDLK_DOWN:
					tempFigure->y+=MOVING_SIZE;
					break;

					case SDLK_LEFT:
					tempFigure->x-=MOVING_SIZE;
					break;

					case SDLK_RIGHT:
					tempFigure->x+=MOVING_SIZE;
					break;
				}

				// update virtual pivot position
				tempFigure->pivotX = tempFigure->x + 50;
				tempFigure->pivotY = tempFigure->y + 50;
		}




		tempFigure=tempFigure->next;
	}

}


void eval_mirror_image()
{
	int e;
	int flagIf=UNABLE;
	int flagIfCorner=UNABLE;
	Figure *tempFigure=first_figure;

			for (e = 0; e < NUM_FIGURES; e++)
			{
					tempFigure->mirrorY = tempFigure->y;
					tempFigure->mirrorX = tempFigure->x;

					if((tempFigure->pivotX == 0 && tempFigure->pivotY == 0) || (tempFigure->pivotX == WIDTH_SCREEN_SIZE && tempFigure->pivotY == 0 ) ||
					(tempFigure->pivotX == WIDTH_SCREEN_SIZE && tempFigure->pivotY == HIGH_SCREEN_SIZE)
					|| (tempFigure->pivotX == 0 && tempFigure->pivotY == HIGH_SCREEN_SIZE))
					{
					flagIfCorner = ENABLE;
					tempFigure->mirror_corner = ENABLE;
					}else
						{
							flagIfCorner = UNABLE;
							tempFigure->mirror_corner = UNABLE;
						}

				if(tempFigure->pivotX == 0)
				{

					tempFigure->mirror = ENABLE;
					tempFigure->mirrorX = WIDTH_SCREEN_SIZE - MOVING_SIZE;

					flagIf = ENABLE;
				}

				if(tempFigure->pivotX == WIDTH_SCREEN_SIZE)
				{

					tempFigure->mirror = ENABLE;
					tempFigure->mirrorX = 	0 - MOVING_SIZE;

					flagIf = ENABLE;
				}

				if(tempFigure->pivotY == 0)
				{

					tempFigure->mirror = ENABLE;
					tempFigure->mirrorY = HIGH_SCREEN_SIZE - MOVING_SIZE;

					flagIf = ENABLE;
				}

				if(tempFigure->pivotY == HIGH_SCREEN_SIZE)
				{

					tempFigure->mirror = ENABLE;
					tempFigure->mirrorY = 	0 - MOVING_SIZE;

					flagIf = ENABLE;
				}

				if(flagIf != ENABLE)
					tempFigure->mirror = UNABLE;

				//check for flagcorner
				if(flagIfCorner != ENABLE)
				{
					tempFigure->mirror_corner = UNABLE;
				}

				tempFigure = tempFigure->next;
			}
}


void eval_position()
{
	int e;
	Figure *tempFigure=first_figure;

			for (e = 0; e < NUM_FIGURES; e++)
			{

			//check for X edge position
				// check to rigth edge of screen
				if(tempFigure->pivotX > WIDTH_SCREEN_SIZE){
					tempFigure->x = 0;
				}

			    // check to left edge of screen
				if(tempFigure->pivotX < 0 ){
					tempFigure->x = WIDTH_SCREEN_SIZE - MOVING_SIZE * 2;
				}

		    //check for y edge position
				// check to upper edge of screen
				if(tempFigure->pivotY > HIGH_SCREEN_SIZE){
					tempFigure->y = 0;
				}

				// check to bottom edge of screen
				if(tempFigure->pivotY < 0 ){
					tempFigure->y = HIGH_SCREEN_SIZE  - MOVING_SIZE * 2;
				}


				tempFigure=tempFigure->next;

			}
}


int main(int argc, char *argv[])
{
	int done = 0;
	init_SDL();
	initialize_figures();
	//display_list();
 	atexit(SDL_Quit);
 	//wait for a hotkey to Exit

 	Uint32 last_time, now_time;

 	draw_figures();
 while(done == 0)
 	{
 		/* Verificar el tiempo al inicio de la iteración */
  		last_time = SDL_GetTicks ();

  		while ( SDL_PollEvent(&event) )
		{

				switch(event.type)
				{
						case SDL_KEYDOWN:

								switch(event.key.keysym.sym)
								{
				    					case SDLK_TAB:
				     					   move_focus();
				     					break;

				     					case SDLK_UP:
				     					   move_figure(SDLK_UP);
				     					break;

				     					case SDLK_DOWN:
				     					   move_figure(SDLK_DOWN);
				     					break;

				     					case SDLK_LEFT:
				     					   move_figure(SDLK_LEFT);
				     					break;

				     					case SDLK_RIGHT:
				     					   move_figure(SDLK_RIGHT);
				     					break;

				   						case SDLK_ESCAPE:
				    						done = 1;
				    					break;
								}

								eval_position();
   								eval_mirror_image();
   								draw_figures();
						break;
					case SDL_QUIT:
						/* Salir de la loop */
						done = 1;
						break;
	   			}


   		}

   		/* Si vamos más rápido que los 24 cuadros por segundo, bajar la velocidad */
   		now_time = SDL_GetTicks ();
		if (now_time < last_time + FPS) SDL_Delay(last_time + FPS - now_time);
   	}
	return EXIT_SUCCESS;
}